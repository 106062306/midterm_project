function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        //var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            //menu.innerHTML = "<a class='dropdown-item' href='profile.html'>" + user.email + "</a><a class='dropdown-item' id='logout-btn' href='signin.html'>Logout</a>";
            document.getElementById('name').innerHTML = user.email;
            
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    var postsRef = firebase.database().ref("world_list");
    var politicsRef = firebase.database().ref("politics_list");
    var businessRef = firebase.database().ref("business_list");
    var entertainmentRef = firebase.database().ref("entertainment_list");
    var techRef = firebase.database().ref("tech_list");
    var sportRef = firebase.database().ref("sport_list");
    var travelRef = firebase.database().ref("travel_list");
    var healthRef = firebase.database().ref("health_list");

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'>";
    var str_middle = "<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray' id='value'><strong class='d-block text-gray-dark'><img src='./images/content.png' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>";
    var str_after_content = "</p></div></div>\n";

    //var postsRef = firebase.database().ref('world_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    var list = document.getElementById("world_list");
    var politics = document.getElementById("politics_list");
    var business = document.getElementById("business_list");
    var entertainment = document.getElementById("entertainment_list");
    var tech = document.getElementById("tech_list");
    var sport = document.getElementById("sport_list");
    var travel = document.getElementById("travel_list");
    var health = document.getElementById("health_list");
    postsRef.once('value')
        .then(function (snapshot) {
            list.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    list.innerHTML += `<p><img src='images/world.jpg' height='30' width='30' class='rounded-circle'> world</p>`;
                    list.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ j + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    list.innerHTML += `<button id="comment_b" onclick="deleteArticle_world('` + i + `')">delete</button>`;
                    list.innerHTML += `<hr class="featurette-divider">`;
                    j++;
                }
            }
        })
        .catch(e => console.log(e.message));
    postsRef.on('value', function (snapshot) {
        list.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    list.innerHTML += `<p><img src='images/world.jpg' height='30' width='30' class='rounded-circle'> world</p>`;
                    list.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ k + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    list.innerHTML += `<button id="comment_b" onclick="deleteArticle_world('` + i + `')">delete</button>`;
                    list.innerHTML += `<hr class="featurette-divider">`;
                    k++;
                }
        }
    })

    politicsRef.once('value')
        .then(function (snapshot) {
            politics.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    politics.innerHTML += `<p><img src='images/politics.jpg' height='30' width='30' class='rounded-circle'> politics</p>`;
                    politics.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ j + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    politics.innerHTML += `<button id="comment_b" onclick="deleteArticle_politics('` + i + `')">delete</button>`;
                    politics.innerHTML += `<hr class="featurette-divider">`;
                    j++;
                }
            }
        })
        .catch(e => console.log(e.message));
    politicsRef.on('value', function (snapshot) {
        politics.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    politics.innerHTML += `<p><img src='images/politics.jpg' height='30' width='30' class='rounded-circle'> politics</p>`;
                    politics.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ k + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    politics.innerHTML += `<button id="comment_b" onclick="deleteArticle_politics('` + i + `')">delete</button>`;
                    politics.innerHTML += `<hr class="featurette-divider">`;
                    k++;
                }
        }
    })

    businessRef.once('value')
        .then(function (snapshot) {
            business.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    business.innerHTML += `<p><img src='images/business.jpg' height='30' width='30' class='rounded-circle'> business</p>`;
                    business.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ j + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    business.innerHTML += `<button id="comment_b" onclick="deleteArticle_business('` + i + `')">delete</button>`;
                    business.innerHTML += `<hr class="featurette-divider">`;
                    j++;
                }
            }
        })
        .catch(e => console.log(e.message));
    businessRef.on('value', function (snapshot) {
        business.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    business.innerHTML += `<p><img src='images/business.jpg' height='30' width='30' class='rounded-circle'> business</p>`;
                    business.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ k + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    business.innerHTML += `<button id="comment_b" onclick="deleteArticle_business('` + i + `')">delete</button>`;
                    business.innerHTML += `<hr class="featurette-divider">`;
                    k++;
                }
        }
    })

    entertainmentRef.once('value')
        .then(function (snapshot) {
            entertainment.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    entertainment.innerHTML += `<p><img src='images/entertainment.jpg' height='30' width='30' class='rounded-circle'> entertainment</p>`;
                    entertainment.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ j + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    entertainment.innerHTML += `<button id="comment_b" onclick="deleteArticle_entertainment('` + i + `')">delete</button>`;
                    entertainment.innerHTML += `<hr class="featurette-divider">`;
                    j++;
                }
            }
        })
        .catch(e => console.log(e.message));
    entertainmentRef.on('value', function (snapshot) {
        entertainment.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    entertainment.innerHTML += `<p><img src='images/entertainment.jpg' height='30' width='30' class='rounded-circle'> entertainment</p>`;
                    entertainment.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ k + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    entertainment.innerHTML += `<button id="comment_b" onclick="deleteArticle_entertainment('` + i + `')">delete</button>`;
                    entertainment.innerHTML += `<hr class="featurette-divider">`;
                    k++;
                }
        }
    })

    techRef.once('value')
        .then(function (snapshot) {
            tech.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    tech.innerHTML += `<p><img src='images/tech.jpg' height='30' width='30' class='rounded-circle'> tech</p>`;
                    tech.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ j + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    tech.innerHTML += `<button id="comment_b" onclick="deleteArticle_tech('` + i + `')">delete</button>`;
                    tech.innerHTML += `<hr class="featurette-divider">`;
                    j++;
                }
            }
        })
        .catch(e => console.log(e.message));
    techRef.on('value', function (snapshot) {
        tech.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    tech.innerHTML += `<p><img src='images/tech.jpg' height='30' width='30' class='rounded-circle'> tech</p>`;
                    tech.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ k + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    tech.innerHTML += `<button id="comment_b" onclick="deleteArticle_tech('` + i + `')">delete</button>`;
                    tech.innerHTML += `<hr class="featurette-divider">`;
                    k++;
                }
        }
    })

    sportRef.once('value')
        .then(function (snapshot) {
            sport.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    sport.innerHTML += `<p><img src='images/sport.jpg' height='30' width='30' class='rounded-circle'> sport</p>`;
                    sport.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ j + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    sport.innerHTML += `<button id="comment_b" onclick="deleteArticle_sport('` + i + `')">delete</button>`;
                    sport.innerHTML += `<hr class="featurette-divider">`;
                    j++;
                }
            }
        })
        .catch(e => console.log(e.message));
    sportRef.on('value', function (snapshot) {
        sport.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    sport.innerHTML += `<p><img src='images/sport.jpg' height='30' width='30' class='rounded-circle'> sport</p>`;
                    sport.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ k + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    sport.innerHTML += `<button id="comment_b" onclick="deleteArticle_sport('` + i + `')">delete</button>`;
                    sport.innerHTML += `<hr class="featurette-divider">`;
                    k++;
                }
        }
    })

    travelRef.once('value')
        .then(function (snapshot) {
            travel.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    travel.innerHTML += `<p><img src='images/travel.jpg' height='30' width='30' class='rounded-circle'> travel</p>`;
                    travel.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ j + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    travel.innerHTML += `<button id="comment_b" onclick="deleteArticle_travel('` + i + `')">delete</button>`;
                    travel.innerHTML += `<hr class="featurette-divider">`;
                    j++;
                }
            }
        })
        .catch(e => console.log(e.message));
    travelRef.on('value', function (snapshot) {
        travel.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    travel.innerHTML += `<p><img src='images/travel.jpg' height='30' width='30' class='rounded-circle'> travel</p>`;
                    travel.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ k + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    travel.innerHTML += `<button id="comment_b" onclick="deleteArticle_travel('` + i + `')">delete</button>`;
                    travel.innerHTML += `<hr class="featurette-divider">`;
                    k++;
                }
        }
    })

    healthRef.once('value')
        .then(function (snapshot) {
            health.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    health.innerHTML += `<p><img src='images/health.png' height='30' width='30' class='rounded-circle'> health</p>`;
                    health.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ j + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    health.innerHTML += `<button id="comment_b" onclick="deleteArticle_health('` + i + `')">delete</button>`;
                    health.innerHTML += `<hr class="featurette-divider">`;
                    j++;
                }
            }
        })
        .catch(e => console.log(e.message));
    healthRef.on('value', function (snapshot) {
        health.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            if(snapshot.val()[i].email == user_email)
                {
                    total_post.push(snapshot.val()[i]);
                    health.innerHTML += `<p><img src='images/health.png' height='30' width='30' class='rounded-circle'> health</p>`;
                    health.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ k + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title + `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                    health.innerHTML += `<button id="comment_b" onclick="deleteArticle_health('` + i + `')">delete</button>`;
                    health.innerHTML += `<hr class="featurette-divider">`;
                    k++;
                }
        }
    })
}

function deleteArticle_world(e) {
    firebase.database().ref('world_list/' + e).remove();
}
function deleteArticle_politics(e) {
    firebase.database().ref('politics_list/' + e).remove();
}
function deleteArticle_business(e) {
    firebase.database().ref('business_list/' + e).remove();
}
function deleteArticle_entertainment(e) {
    firebase.database().ref('entertainment_list/' + e).remove();
}
function deleteArticle_tech(e) {
    firebase.database().ref('tech_list/' + e).remove();
}
function deleteArticle_sport(e) {
    firebase.database().ref('sport_list/' + e).remove();
}
function deleteArticle_travel(e) {
    firebase.database().ref('travel_list/' + e).remove();
}
function deleteArticle_health(e) {
    firebase.database().ref('health_list/' + e).remove();
}
window.onload = function () {
    init();
};